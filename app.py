from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def home():
    return "This is 1610706093 Samapong Pongrakthai"

@app.route('/beasts')
def welcome():
    return render_template('beasts.html')

if __name__ == '__main__':
    app.run(debug=True)
